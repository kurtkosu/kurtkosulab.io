# ISSessions Website Repository
### ISSessions.ca is built using the static site generator [Hugo](https://gohugo.io/) & hosted  on [GitLab Pages](https://pages.github.com/)

- All the files in this repository are what we use to build the website. 
- Anytime this repository is modified, GitLab Pages rebuilds the site using Hugo.
- **There is no back-end to this website.** There is no CMS. What you see is what you get.

## Our goals with this set-up are as follows:

- **A modern website that is flexible & free**
	- Hugo gives us great flexibility 
	- GitLab Pages free tier is excellent
- Website contributions from anyone should be easy
	- Entire website is public!
- Anyone can clone and run their own offline version (in minutes!)
	- Follow our getting started guide below
- Easy to maintain, especially between executive transfers
	- Managing multiple accounts is gross.

## Getting Started with your own offline version (no git knowledge required)

- ## Part 1: Install Hugo
	Download **Hugo Extended Edition** for your OS. Follow the guide below.
	https://gohugo.io/getting-started/installing/

	> We require the extended edition to compile sass to css.
	
	- #### Note for Windows 10 Users once hugo is installed:[](https://gohugo.io/getting-started/installing/#for-windows-10-users)
		- Right click on the  **Start**  button.
		- Click on  **System**.
		-  Click on  **Advanced System Settings**  on the left.
		-  Click on the  **Environment Variables…**  button on the bottom.
		-  In the User variables section, find the row that starts with PATH (PATH will be all caps).
		-  Double-click on  **PATH**.
		-  Click the  **New…**  button.
		-  Type in the folder where  `hugo.exe`  was extracted, which is  `C:\Hugo\bin`  if you went by the instructions above.  _The PATH entry should be the folder where Hugo lives and not the binary._  Press Enter when you’re done typing.
	 	- Click OK at every window to exit.

- ## Part 2: Download the website source code

	- In the [repository](https://gitlab.com/kurtkosu/kurtkosu.gitlab.io
), click the download button next to the clone button. 
	- Unzip the folder to the location of your choosing
	- In a command prompt, move to that un-zipped folder directory and run the command:
	    > hugo serve
	- Open your web browser and direct yourself to the localhost address that is given. (default http://localhost:1313/)
