---
firstName: "Jamie"
lastName: "Paugh"
photo: "jamie-paugh.jpeg"
social:
    linkedin: "jamie-paugh"
executive:
    - 2020:
        position: Event Manager
---