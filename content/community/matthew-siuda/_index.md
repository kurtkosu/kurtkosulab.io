---
firstName: "Matthew"
lastName: "Siuda"
photo: "matthew-siuda.jpeg"
social:
    twitter: suedeSecurity
    linkedin: "matthew-siuda-743b9615a"
---