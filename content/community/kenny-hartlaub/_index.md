---
firstName: "Kenny"
lastName: "Hartlaub"
---
**Kenny Hartlaub** is an Associate Consultant on FireEye Mandiant’s Red Team. He has been in the security field for 4 years as a penetration tester and red teamer. Kenny graduated from the ISS program in 2018 and is very passionate about learning and growing in his field. He has come to know failures and successes over his time to where he is now and uses his experiences to help others succeed. 

