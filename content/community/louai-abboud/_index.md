---
firstName: "Louai"
lastName: "Abboud"
speaker: true
photo: "louai-abboud.jpg"
social:
    twitter: "louailoopdidoop"
    linkedin: "louaiabboud"
executive:
    - 2020:
        position: Vice President
    - 2019:
        position: President
---