---
firstName: "Andrew"
lastName: "Major"
---
**Andrew Major** is a Consultant on FireEye Mandiant’s Incident Response Team. He graduated from the ISS program in 2018. After not having an idea of what to do at the end of year 3, a forensics lab accepted him for a co-op. Now with two years on Mandiant’s IR team fighting against APT groups, he is here to encourage others to get into the same rewarding field.