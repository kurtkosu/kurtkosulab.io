---
id: "kurt kosubek"
firstName: "Kurt"
lastName: "Kosubek"
social:
    twitter: "kurtkosubek"
    linkedin: "kurtkosubek"
    github: "kurtkosu"
roles:
    - executive:
        - 2020:
            postion: president
---

Kurt is a pretty cool guy