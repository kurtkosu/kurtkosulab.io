---
firstName: "Olivier"
lastName: "Lemelin"
---
Olivier is currently a Threat Hunter at a major Canadain Telco and has been a part of the security industry for the last five years. He believes that the modern web browser is too complex and could be replaced by a well-behaved terminal environment and a bunch of Gopher servers. In his free time, he enjoys binary exploitation, Emacs, LISP programming, and gardening, all from the comfort of his $HOME.