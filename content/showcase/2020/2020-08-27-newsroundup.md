---
title:  "News Roundup - August 27th"
publishDate: "2020-08-27"
image: /images/newspaper.svg
community:
    - Yusef Islaih
    - Nashad Hakim
tableOfContents: true
---


{{< news
news-title="Safari Bug Revealed"
news-summary="Safari Bug Revealed After Apple Takes Nearly a Year to Patch"
news-image="https://media.threatpost.com/wp-content/uploads/sites/103/2020/08/25105326/Safari_Browser.jpg"
float="right"
news-site="ThreatPost"
news-source="https://threatpost.com/safari-bug-revealed-after-apple-takes-nearly-a-year-to-patch/158612/"
>}}


* Security hole could leak files
* Safari’s implementation of Web Share API 
* Allows access to files stored on user’s local hard drive 
* Link is passed to the navigator.share function
* Problem is not very serious
* Researcher had much back and forth with Apple 
    * Apple said they would resolve it in Spring 2021
    * Prompted researcher to revel his findings


<hr>

<h2 data-toc-text="North Korean Malware"> CISA warms of BLINDINGSCAN, a new North Koreawn malware </h2>

<div class="image-container" style="float:left;">
<img src="https://zdnet3.cbsistatic.com/hub/i/2019/08/13/874ba233-9e30-417b-ae47-e6062ead7bce/north-korea-reportedly-stole-2b-in-wave-5d4d934316e22d00012a3ac5-1-aug-13-2019-12-43-01-poster.jpg" alt=""> 
<span class="source text-muted">  
    Source: <a href="https://www.zdnet.com/article/cisa-warns-of-blindingcan-a-new-strain-of-north-korean-malware/"> ZDNet</a>
</span>
</div>


* New malware spotted in attacks targeting US and foreign companies 
* North Korean hackers posing as recruiters and asked victims to go through the interview process 
* Payload is a Remote Access Trojan (RAT) aka BLINDINGCAN hidden in Office or PDF documents
* Hackers used malware to gain access to victim's systems, perform reconnaissance, and then "gather intelligence surrounding key military and energy technologies."


<hr>

<h2 data-toc-text="Twitter Bans Dracula Botnet"> Twitter takes down 'Dracula' botnet pushing pro-Chinese propaganda </h2>
<div class="image-container" style="float:right;">
<img src="https://zdnet3.cbsistatic.com/hub/i/2020/08/26/2b21a1c4-99bf-4a05-a73f-1642da149db2/dracula-botnet.png" alt=""> 
<span class="source text-muted">  
    Source: <a href="https://www.zdnet.com/article/twitter-takes-down-dracula-botnet-pushing-pro-chinese-propaganda/"> ThreatPost</a>
</span>
</div>

* Graphika - found 3000 bots
* Accounts used quotes from Bram Stoker's Dracula book 
* Discovered early unlike other incidents
* Accounts were able to get predetermined topics trending
* Suspended through twitters algorithms or manually? 



<hr>

<h2 data-toc-text="Medical Data Github Leak"> Medical Data Leaked on GitHub Due to Developer Errors </h2>
<div class="image-container" style="float:left;">
<img src="https://media.threatpost.com/wp-content/uploads/sites/103/2020/08/26094717/Medical-Data.jpg" alt=""> 
<span class="source text-muted">  
    Source: <a href="https://threatpost.com/medical-data-leaked-on-github-due-to-developer-errors/158653/"> ThreatPost</a>
</span>
</div>


* Developer error caused leak PHI of over 150,000 patient records
* Researchers wanted to see if there were medical customer data on GitHub. (< 10 minutes to find) 
* Researchers reported breach to DataBreaches.net to get victims attention. Article Title "No Hack When It's Leaking" 
* Errors undetected for months due to negligent security policies


<hr>

<h2 data-toc-text="Cisco Patches Bugs"> Cisco Patches ‘High-Severity’ Bugs Impacting Switches, Fibre Storage </h2>
<div class="image-container" style="float:right;">
<img src="https://media.threatpost.com/wp-content/uploads/sites/103/2020/08/26153842/Cisco_Systems_Sign.jpg" alt=""> 
<span class="source text-muted">  
    Source: <a href="https://krebsonsecurity.com/2020/04/how-cybercriminals-are-weathering-covid-19/"> ThreatPost</a>
</span>
</div>

* Bugs impacted network gear including switches and fibre storage
* Ciscos's NX-OS hit
    * Impacts nexus-series ethernet switches and MDS-series fibre channel storage
* CVE-2020-3397
    * Due to incomplete input validation
* CVE-2020-3398 
    * Due to incorrect parsing
* Others would allow DoS attacks remotely and injecting arbitrary code
* 9 switches impacted overall


<hr>

<h2 data-toc-text="SANS ORG Data Breach"> SANS infosec training org suffers data breach after phishing attack </h2>
<div class="image-container" style="float:left;">
<img src="https://www.bleepstatic.com/content/posts/2020/08/sans-phishing.jpg" alt=""> 
<span class="source text-muted">  
    Source: <a href="https://www.bleepingcomputer.com/news/security/sans-infosec-training-org-suffers-data-breach-after-phishing-attack/"> BleepingComputer</a>
</span>
</div>


* SANS employee fell for a phishing attack giving access to their email account
* Discovered during a review of their email configuration
* Rule configured to forward all email received (513 emails containing 28,000 of PII) to an "unknown external email address" and installed a malicious Office 365 add-on
* Will be using this as an educational oppertunity for themselves as well as the community


<hr>


<h2 data-toc-text="Lazarus Group Targets Cryptocurrency"> Lazarus Group Targets Cryptocurrency Firms Via LinkedIn Messages </h2>
<div class="image-container" style="float:right;">
<img src="https://media.threatpost.com/wp-content/uploads/sites/103/2019/02/04150137/APT_Lazarus_APT1.png" alt=""> 
<span class="source text-muted">  
    Source: <a href="https://threatpost.com/lazarus-group-targets-cryptocurrency-firms-via-linkedin-messages/158614/"> ThreatPost</a>
</span>
</div>


* Lazarus targeting admins at cryptocurrency firms via phishing messages on linked in
* How does it work?
    * Sends phishing, enable macros in Microsoft Word, mshta.exe, bit.ly link, VBScript, execution of PowerShell 
* Ability to download, decompress, C2 communication, execute commands
* Many command executed would give blue teams big leads



<hr>

<h2 data-toc-text="AWS Virtual Machine Infected"> An AWS Virtual Machine Is Infected With Mining Malware. There Could Be Others </h2>
<div class="image-container" style="float:left;">
<img src="https://static.coindesk.com/wp-content/uploads/2020/08/Mitgo-710x458.jpg?format=webp" alt=""> 
<span class="source text-muted">  
    Source: <a href="https://www.coindesk.com/aws-mining-malware-cryptojacking-monero"> CoinDesk</a>
</span>
</div>


* AWS AMI for a Windows 2008 virtual server infected with a Monero mining script
* Trend of “crypto-jacking” attacks
* AWS’s documentation includes the caveat that users choose to use Community AMIs “at [their] own risk” and that Amazon “can’t vouch for the integrity or security of [these] AMIs.” 
* Monero is the coin of choice due to its mining algorithm
* What else is hidden? Backdoor? Ransomware?


<hr>