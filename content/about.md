---
title: About ISSessions
aliases:
  - /about/
---

<!-- About section -->
<div class="container marketing">

  <div class="row featurette">
    <div class="col-md-12">
      <h2 style="color:$green;">A Community of InfoSec Enthusiasts</h2>
      <p>ISSessions is an information security meetup group that features a security news roundup as well as talks by students and industry professionals. 
        In addition, we host a number of technical workshops and trainings on various information security topics. Our biggest event of the year is the Sheridan CTF which attracts over a 100 participants from various InfoSec programs across Ontario. Our mission is to develop students into highly-skilled cybersecurity professionals who can defend corporate networks and critical infrastructure against online threats.</p>
    </div>
      
  </div>
<hr>

  <div class="row featurette">
    <div class="col-md-6 order-md-1">
      <h2 style="color:$green;">Join our community!</h2>
        <p>Our meetings take place at Sheridan College, Trafalgar Campus, 
            located at 1430 Trafalgar Rd, Oakville, ON L6H 2L1 in Room J102.</p>
      <!-- Begin Mailchimp Signup Form -->
      <button type="button" class="btn" data-toggle="modal" data-target="#exampleModalCenter">
        Sign up for our mailing list!
      </button>   
      <a href="https://discord.gg/QJaK3VAfed" class="btn">Join our Discord server!</a>
      <!-- Modal -->
      <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">ISSessions Mailing List</h5>
              <button class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form action="https://issessions.us8.list-manage.com/subscribe/post?u=1b872b4e25d5eacc026bf6423&amp;id=75e13fb502"
                  method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate"
                  target="_blank" novalidate>
              <div class="modal-body">
                  <div class="form-group">
                    <label for="mce-EMAIL">Email Address *</label><br>
                    <input type="email" class="form-control" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Enter email">
                  </div>
                  <div class="form-group">
                    <label for="mce-FNAME">First Name *</label>
                    <input type="text" class="form-control" value="" name="FNAME" class="required" id="mce-FNAME" placeholder="First Name">
                  </div>
                  <div class="form-group">
                    <label for="mce-LNAME">Last Name </label>
                    <input type="text" class="form-control" value="" name="LNAME" class="" id="mce-LNAME" placeholder="Last Name">
                  </div>
                  <div id="mce-responses" class="clear">
                    <div class="response" id="mce-error-response" style="display:none"></div>
                    <div class="response" id="mce-success-response" style="display:none"></div>
                  </div>
                   <div class="indicates-required">
                    <small> * indicates required</small>
                  </div>
                  <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                  <div style="position: absolute; left: -5000px;" aria-hidden="true">
                    <input type="text" name="b_1b872b4e25d5eacc026bf6423_75e13fb502" tabindex="-1" value="">
                  </div>
                <div class="modal-footer" style="justify-content: center;">
                  <button type="submit" class="btn" value="Subscribe" name="subscribe" id="mc-embedded-subscribe"> Subscribe!
                </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <!--End mc_embed_signup-->
    </div>
    <!-- Contact -->
    <div class="col-md-6 order-md-2">
      <h2 style="color:$green;">Need to get in touch?</h2>
      <a href="https://forms.gle/Yz6QJHiyAijTvsHn9">
      <button type="submit" class="btn">
        Contact our team!
      </button></a>
    </div>
  </div>
</div>


<!-- CCR section -->

