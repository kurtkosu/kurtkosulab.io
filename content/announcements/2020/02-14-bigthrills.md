---
title:  "Big Thrills, Bigger Events"
publishDate: "2020-02-14"
community:
    - Johnathan Wong
summary: "As midterms approach, we also have many BIG TICKET events coming up. This post should serve as a good reference for dates and times - it will be updated as more details come to light!"
---


<p>Good morning everyone,<br><br>As midterms approach, we also have many BIG TICKET events coming up. This post should serve as a good reference for dates and times - it will be updated as more details come to light! First, two things to arrive in the near future:<br><br>1. Please watch the Discord announcements this week and next week. There will be a lot of announcements concerning ISSessionsCTF2020 on March 14 @ Trend Micro Office in Toronto. Spots are limited. You will want to be well-informed on any news and information surrounding the event. Additionally, forming teams of 4 might be beneficial in the long run.  Registration opens on <strong>Monday Feb 17. </strong>And please I humbly ask you to register only if you are 110% sure you will attend. The Eventbrite link + a detailed breakdown of event day will be posted on Friday.</p>

<p>2. For those
unaware, Lee Kagan had some unforeseen difficulties on our original
planned date.  We are happy to announce that the Command and Control
workshop has been rescheduled to Feb 28 8:30AM-5:00PM with the
pre-workshop setup happening on Feb 27, 7:00-9:00PM. There is a new
Eventbrite so kindly register again! Food (likely
pizza/coffee/snacks) will be provided!</p>

<p>You can register for that specific event here:<br><a href="https://www.eventbrite.ca/e/lee-kagan-building-command-control-servers-c2s-workshop-advanced-track-tickets-94055286895">https://www.eventbrite.ca/e/lee-kagan-building-command-control-servers-c2s-workshop-advanced-track-tickets-94055286895</a></p>

<p>Please note the
following important dates for events during this semester:</p>

<p>[Monday, February
17, 9:00AM @ Eventbrite] - ISSessionsCTF2020 Registration Opens</p>

<p>[Thursday, February
27, 7:00PM-9:00PM @ Room J102] - Command &amp; Control Pre-Workshop
Setup</p>

<p>[Friday, February
28, 8:30AM - 5:00PM @ J102] - Command &amp; Control Workshop</p>

<p>[Saturday, March 6,
7:00PM @ J102] - Special Meeting: How-To-CTF!</p>

<p>[Saturday, March 14,
8:30AM @ Trend Micro Office, Toronto] - ISSessionsCTF2020</p>

<p>[Saturday, April 4, 8:30AM-5:30PM @ Rooms TBD] - ISSessions Workshop Day #2, Beginner Track [Intro to Soldering by Nick, Building a DIY Rubber Ducky by Richard and Daniel], Advanced Track [Intro to Assembly by John Simpson, N-Day Vulnerability Researcher at Trend Micro]</p>
