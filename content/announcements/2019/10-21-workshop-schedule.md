---
title:  "Workshop Schedule and more!"
publishDate: "2019-10-21"
community:
    - Johnathan Wong
summary: "The ISSessions club is proud to announce that we have planned out our workshop schedule for the Fall term.  Many thanks to our volunteer hosts for each workshop as these would be happening without you"
---

The ISSessions club is proud to announce that we have planned out our workshop schedule for the Fall term.  Many thanks to our volunteer hosts for each workshop as these would be happening without you.  The schedule is as follows: 

* Meeting + Mini-Worskshop: An Intro to Git – Friday, November 1, 7:00PM-9:30PM By Adam Burek 
* Smashing the Web: SQL Injection & Cross-Site Scripting – Friday, November 8, 7:00PM-9:30PM By Spencer Lee 
* Beware the Malware: A Gentle Introduction To Malware Analysis Series #1 – Wednesday, November 13, 7:00PM-9:30PM By Jason Hong and Ken Onuralp 
* An Intro to Red Teaming – Saturday, November 23, 9:30AM-5:30PM By Benjamin Maher, Director of Advisory, Security Compass 
* Beware the Malware: A Gentle Introduction To Malware Analysis Series #2 – Wednesday, November 27, 7:00PM-9:30PM By Jason Hong and Ken Onuralp 

Next semester, we will have more workshops on Assembly, Lockpicking, Soldering, and so much more!! 

Don't forget: we will still be having meetings on Nov 15 and Nov 29. 

