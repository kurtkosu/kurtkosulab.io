---
title:  "Soldering Workshop"
date:   "2018-12-07T19:00:00-0400"
location: "Room S302, Sheridan Trafalgar Campus, Oakville"
publishDate: "2019-09-26"
community:
    - Nick Johnston
---

ISSessions will be hosting a soldering workshop on December 7th! If you've ever wanted to learn how to solder, now is the time to do it.If you're interested, you can sign up here:

https://goo.gl/forms/VQsVLWtsSZLXYmKN2

You can also choose to get a 14 piece soldering kit for an additional fee when you register, a complete list of what you'll get can be found here

That's it for now, we hope to see you all out there,
-ISSessions