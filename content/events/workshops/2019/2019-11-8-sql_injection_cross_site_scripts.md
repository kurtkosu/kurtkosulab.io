---
title:  "SQL Injection & Cross-Site Scripting"
date:   "2019-11-08T19:00:00-0400"
location: "Room J102, Sheridan Trafalgar Campus, Oakville"
publishDate: "2019-11-07"
community:
    - Spencer Lee
---


<p>As we return back to Standard Time in Canada, our next workshop is slowly approaching on Friday, November 8th. </p>

<p>Smashing the Web is our next workshop being led by Spencer Lee on Friday November 8. This workshop will cover SQL injection (SQLi) and cross site scripting (XSS), spanning introductory examples to complex real world problems. </p>

<h2>Software and Hardware:</h2>

<p>You will need the following ready and running for the workshop:</p>

<ul><li>A laptop with Linux running natively or in a virtual machine<ul><li>The Linux host or VM will also need to have docker and docker-compose installed as well as any web browser (Firefox, Chrome, etc.)</li></ul></li></ul>

<p>For simplicity, we’ve created the following VMWare Virtual Machine image. It is pre-loaded with all the tools you need. Please download it BEFORE the workshop (it's ~ 3.9G!). We will be going over how to load it into VMWare during the workshop (so please make sure you have VMWare Player or VMWare Workstation installed by then). </p>

<p>Download Link to Virtual Machine: <a href="https://drive.google.com/file/d/1g9y8fhZ5a-Qsz7HHxsAcDRNNRqCl-MWE/view?usp=sharing">https://drive.google.com/file/d/1g9y8fhZ5a-Qsz7HHxsAcDRNNRqCl-MWE/view?usp=sharing</a></p>

<p>Our rough guide for the entire workshop is as follows:<br>First half – SQL<br>   Intro to basic SQL<br>   Minimal PHP syntax coverage<br>   Overview of injecting SQL into &lt;input&gt; tags<br>   four login examples<br>   two search bar examples<br>Second half  - XSS<br>    Installing local website with docker (github link will be provided)<br>    Overview of XSS scripting<br>    Introduction to Basic Javascript<br>    XSS demonstration<br>    Using local the local web server<br>         -  we’ll practice many different XSS attacks in partners </p>
