---
title:  "Introduction to Red Teaming"
date:   "2019-11-23T9:30:00-0400"
location: "Room J102, Sheridan Trafalgar Campus, Oakville"
publishDate: "2019-11-17"
community:
    - Benjamin Mahar
---

<p>This is a reminder that our next workshop is "Introduction to Red Teaming" by Mr. Benjamin Mahar who is the Director of Advisory at Security Compass. Ben will delve into a series of hands-on exercises that walk students though the red teaming process from initial access to full network compromise on a practice environment. </p>

<p>As per our usual meetings, it will take place at...<br>Location: Room J102 <br>Time: Saturday, Nov 23, 9:30AM - 5:30PM</p>

<p><strong><span style="text-decoration: underline;">Prerequisites:</span></strong><br>This is an advanced workshop. While the tools themselves are not complex, the underlying concepts can be quite daunting for beginners. You will need to have a good understanding of:</p>

<ul><li>Networking (TCP/IP, IP routing, VPNs, network scanning, OS fingerprinting, proxies) </li><li>Windows (RDP, Scheduled Tasks, Registry, CMD, DLLs, WMI, etc.) </li><li>Basic JavaScript (Event Listeners, XMLHTTPRequest, Browser Profiling) </li><li>Basic Cryptography (Public &amp; Private Keys, Certificates, etc.)</li><li>nmap </li><li>Metasploit, Meterpreter, Searchsploit</li><li>OpenVPN</li></ul>

<p><strong><span style="text-decoration: underline;">Requirements:</span></strong></p>

<ol><li>Download and Install Kali Linux in a VM </li><li>Open a terminal and run: </li><li>sudo apt-get update &amp;&amp; sudo apt-get upgrade </li><li>(Optional) I also strongly suggest you play with nmap and Metasploit before the workshop. Cheatsheets may be provided to the best of our ability.</li></ol>

<h2>IMPORTANT NOTES:</h2>

<p><strong>If the prep work is not completed, you will NOT be allowed to participate. As such, please come prepared.</strong></p>

<p>We will be opening up 40 seats for this workshop. Given the amount of knowledge required, we will be restricting the number of tickets allocated to first year students to 8. Tickets will be available (for free) on Eventbrite. If you are a first year student, ensure you select "First Year Admission". Otherwise, select "Upper Year/Alumni Admission." We will be checking OneCards/IDs at the door to verify your year.  Tickets are first come, first serve. They will be released at 11:30AM Monday morning.</p>

<p>Please use the following link to sign up for the workshop. <br><a href="http://bit.ly/iss-redteam">iss-redteam sign up</a></p>
