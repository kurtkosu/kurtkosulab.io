---
title:  "Pandas (Python) Workshop"
date:   "2020-12-05T13:00:00-0400"
publishDate: "2020-12-01"
community:
    - Louai Abboud
    - Jamie Paugh
    - Nashad Hakim
    - Kurt Kosubek
    - Yusef Islaih
    - Cem Celen
image: /images/workshop.svg
location: "Virtual (Discord & Youtube)"
featureSize: 1
showPostSummary: true
summary: "Louai Abboud guides you through the Data Analysis library, Pandas!"
---
**Date:** December 5th, 2020  
**Time:** 1:00-4:00 PM EST  
**Location:** Online (Zoom)  
**Years:** 1-4 (ANY YEAR IS WELCOME) 

<div class="image-container" style="margin:0">
    <img src="../images/pandas_workshop.png" style="margin-left:0">
</div>

**Description:** Pandas is a Python Data Analysis library! In security, we deal with tons of data on a daily basis: logs, ticket data, user behaviour analytics, etc. Pandas is crucial in analyzing that data quickly and automating security processes for you. Alongside that, Pandas is a great python library for anyone interested in Machine Learning; note that this is not an ML workshop, but Pandas is used in ML. 

This is an introductory workshop, but you will quickly pick up the concepts and learn a lot regardless of your year. You DO NOT need to know python for this workshop, so don’t let that stop you from signing up.   If any of that appealed to you, SIGN UP! 

**Requirements:** Windows machine with VMWare installed. (MacOS or Linux allowed – use at your own risk)  
**Sign-up Link:** https://www.eventbrite.ca/e/issessions-tickets-130791505915