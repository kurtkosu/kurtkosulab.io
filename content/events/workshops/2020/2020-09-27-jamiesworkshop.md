---
title:  "Beginners Bash Workshop"
date:   "2020-09-27T11:00:00-0400"
publishDate: "2020-09-20"
community:
    - Jamie paugh
    - Kurt Kosubek
    - Yusef Islaih
    - Cem Celen
image: /images/workshop.svg
location: "Virtual (Discord & Youtube)"
featureSize: 1
showPostSummary: true
summary: "Jamie Paugh demystifies working with BASH"
---
ISSessions is holding our first workshop of the year, and you won’t want to miss it (especially first and second years)! The workshop is scheduled for Saturday, October 3rd, from 11 am – 2:00 pm (we may go up to an hour later). This event is entirely FREE for you!

<u><strong>Beginner’s BASH</strong></u>  
Beginner’s BASH will provide a primer for UNIX shell environments to help demystify the workflow and give beginners the ability to become productive in a command-line environment. This will be accomplished via Zoom where our event manager Jamie Paugh goes through “OverTheWire” lab Bandit levels 0-16. Members can ask questions and code-along live with the stream. There is no BASH experience required and we encourage those with experience to come by and improve upon their skillbase.

<div class="image-container" style="margin:0">
    <img src="../images/bash_workshop.jpg" style="margin-left:0">
</div>

You will use BASH a lot in the future, so why not get ahead of the curb while you can!

**When:** October 3rd, 2020 11:00 AM Eastern Time (US and Canada)  
Register in advance for this meeting: https://us02web.zoom.us/meeting/register/tZctd-2rrz4qGdIuN0wSpmYDVj5_6Ww7NN4p  
After registering, you will receive a confirmation email containing information about joining the meeting.

Get ready for a great workshop and we hope to see many first and second years there!

– ISSessions Exec Team