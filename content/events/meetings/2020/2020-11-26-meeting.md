---
title: "Virtual ISSessions Meeting"
date: "2020-11-26T19:00:00-0400"
publishDate: "2020-11-23"
location: "Virtual (Discord & Youtube)"
image: /images/discord_logo.svg
featureSize: 1
community:
  - Kenny Hartlaub
  - Andrew Major
  - Yusef Islaih
  - Nashad Hakim
  - Louai Abboud
  - Cem Celen
  - Jamie Paugh
  - Kurt Kosubek

showPostSummary: true
summary: "New segment and special guests Kenny H. & Andrew M."
---
Date: 2020-11-26 (THURSDAY)   
Time: 19:00 – 21:00 EST  
Location: VIRTUAL (Discord + YouTube)  

<u>**Agenda**</u>   
19:00 – Kickoff – Kurt 🔑  
19:10 – News Roundup – Louai & Yusef 🗞  
19:30 – Memory Lane – Cem & Kurt 👀   
19:55 – Break  🛌   
20:00 – Breaking into Red Team & Incident Response – Kenny Hartlaub & Andrew Major 👑 👨‍🎓  

{{< youtube QnsW8RwF170 >}}

COME ONE, COME ALL!! Ladies and gentlemen, security students and pros, hobbyists and newbz!

We are in for a delicacy of a treat this meeting… and it’s going to be one for the history books! Join us this Thursday, November 26, @ 7 PM, for the latest news in InfoSec, a newly added segment, and some extraordinary guest speakers (PLURAL).

Starting off, Louai and Yusef tackle the latest news in InfoSec. Next, Kurt and Cem take a dive into the historical significance and impact of an exploit in a new segment we’re calling “Memory Lane”. Want to know how and why the notorious Buffer Overflow became so infamous? Join this deep dive – we guarantee you’ll learn something you never knew! To end off a great night, two ISS alumni, Kenny Hartlaub & Andrew Major – who currently work at Mandiant – give us the ins and outs of their Red Teaming and IR careers.

***

<u>**Breaking into Red Team & Incident Response – by Kenny Hartlaub & Andrew Major**</u>   
Mandiant pros Kenny Hartlaub and Andrew Major steer through the fascinating ride that is Red Teaming and IR. The ups, the downs, the journey to and the maintenance after, no stone will be left unturned as you peek over the shoulders of two industry pros. They will also be giving tips, tricks and advice on preparing to embark on these insanely cool career paths! 

**Kenny Hartlaub** is an Associate Consultant on FireEye Mandiant’s Red Team. He has been in the security field for 4 years as a penetration tester and red teamer. Kenny graduated from the ISS program in 2018 and is very passionate about learning and growing in his field. He has come to know failures and successes over his time to where he is now and uses his experiences to help others succeed. 

**Andrew Major** is a Consultant on FireEye Mandiant’s Incident Response Team. He graduated from the ISS program in 2018. After not having an idea of what to do at the end of year 3, a forensics lab accepted him for a co-op. Now with two years on Mandiant’s IR team fighting against APT groups, he is here to encourage others to get into the same rewarding field.

***

As the snow and cold really hit 🥶, come out (virtually) on Thursday for this superb night! See y’all there 😉 

– ISSessions Exec Team 💯