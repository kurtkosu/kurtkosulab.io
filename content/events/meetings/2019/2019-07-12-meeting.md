---
title:  "ISSessions Meeting"
date:   "2019-07-12T19:00:00-0400"
publishDate: "2019-07-6"
location: "Room J102, Sheridan Trafalgar Campus, Oakville"
community:
    - Johnathan Wong
    - Louai Abboud
    - Nick Johnston
    - Adam Burek
    - Adam Greenhill

summary: "The guest speaker for this session is Adam Greenhill.  He is a senior security consultant at Security Compass and has done talks at BSides Toronto, OWASP Toronto, Toronto's Cyber Security Meetup, and Sheridan College's ISSessions. "
---

<p>Hopefully the last few day's of dangerously hot weather hasn't worn you out.   Our next ISS Sessions meeting will be on July 12th, 2019 from 7-8:45 in  J102 at the Trafalgar Campus. Our rough agenda is as follows: </p>

<p>Date: 2019-07-12<br>
Time: 19:00 - 20:45<br>
Location: Room J102, Sheridan Trafalgar Campus, Oakville<br>
Agenda<br>
* 19:00 - Announcements with Adam<br>
* 19:05 - Infosec News Roundup and Open Discussion with your hosts Nick and Adam<br>
* 19:25 - Resource of the Month with Nick<br>
* 19:30 - In-Depth Feature Story with Adam<br>
* 20:00 - Guest Speaker Adam Greenhill on CSV Injection<br>
* 20:45 - Wrap and Refreshments (across the street at the pub)</p>

<p>The guest speaker for this session is Adam Greenhill.  He is a senior security consultant at Security Compass and has done talks at BSides Toronto, OWASP Toronto, Toronto's Cyber Security Meetup, and Sheridan College's ISSessions. </p>

<p>The topic for the presentation will be on CSV injection.  CSV is a common format for data to be exported for both analysis and statistical purposes.  We'll be discussing injection, prevention, the danger regarding the lack of input sanitation and more.</p>

<p>I've taken the liberty of also adding the ISSessions Linkedin to the sidebar as well.</p>



<p>Have a good weekend and see you all next week!</p>
