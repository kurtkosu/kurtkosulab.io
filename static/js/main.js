//Script for navbar shrinking on scrolldown

window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 70 || document.documentElement.scrollTop > 70) {
    document.getElementById("navbar").style.paddingBottom = "0px";
    document.getElementById("navbar").style.paddingTop = "0px";
    
  } else {
    document.getElementById("navbar").style.paddingBottom = "8px";
    document.getElementById("navbar").style.paddingTop = "8px";
  }
}
